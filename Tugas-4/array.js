//Soal No. 1 Array Multidimensi
console.log("Soal No. 1 - Array Multidimensi");
console.log()
var input = [
    ["0001", "Roman alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function datahandling(n) {
    for (var i = 0; i < n.length; i++) {
        console.log("Nomor ID : ", n[i][0])
        console.log("Nama Lengkap : ", n[i][1])
        console.log("TTL : ", n[i][2], n[i][3])
        console.log("Hobbi : ", n[i][4])
        console.log("");
    }
}
datahandling(input);

//Soal No. 2 Balik Kata
console.log("Soal No. 2 Balik Kata");
console.log()
function balikKata(string) {
    var output = ""
    var pstring = string.length
    for (var a = pstring-1; a >= 0; a--) {
        output += string[a]
    }
    return output;    
}
console.log(balikKata("SanBerCode"));
console.log(balikKata("racecar"));
console.log(balikKata("kasur rusak"));
console.log(balikKata("haji ijah"));
console.log(balikKata("I am Sanbers"))
