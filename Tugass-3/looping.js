//looping dalam JavaScript dengan "while"
console.log("Soal No. 1");
console.log("OUTPUT PERTAMA");
var i = 2;
while (i <= 20){
    var output1 = i + " - I love coding";
    console.log(output1);
    i += 2
}
console.log("OUTPUT KEDUA");
var j = 20;
while (j >= 2){
    var output2 = j  + " - I will become a mobile developer";
    console.log(output2);
    j -= 2
}
console.log();

//looping menggunakan "for"
console.log("Soal No. 2");
for (var k = 1; k <= 20; k++) {
    var hasil = "";
    if (k % 2 == 1) {
        if (k % 3 == 0) {
            hasil = k + " - I Love Coding";
        } else {
            hasil = k + " - Santai ";
        }
    }   else {
        hasil = k + " - Berkualitas ";
    }
    console.log(hasil);
}
console.log();

//Membuat persegi panjang #
console.log("Soal No. 3");
function makeRectangle (panjang, lebar) {
    for (var p = 1; p <= lebar; p++) {
        var baris = "";
        for (var l = 1; l <= panjang; l++) {
            baris += "#";
        }
        console.log(baris);
    }
}
makeRectangle(8,4);
console.log();

//Membuat Tangga
console.log("Soal No. 4");
function makeladder(sisi){
    for (r = 1; r <= sisi; r++){
        hasil1 = "";
        for (t = 1; t <= r; t++){
            hasil1 +="#";

        }
        console.log(hasil1);
    }
}
makeladder(7);