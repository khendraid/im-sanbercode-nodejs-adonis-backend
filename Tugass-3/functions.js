console.log("Soal No. 1");
function teriak(){
    console.log("Halo Sanbers!");
}
teriak();
console.log();

//Perkalian 2 parameter
console.log("Soal No. 2")
function kalikan(num1, num2){
    return num1 * num2
}
console.log(kalikan(4, 12));
console.log();

//Proses parameter menjadi sebuah kalimat
console.log("Soal No. 3")
function introduce(name, age, address, hobby){
    return ("Nama saya " + name + ", umur saya " + age + " tahun, " + "alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby) 
}
console.log(introduce("Agus", 30, "Jogja", "Gaming!"));
console.log(introduce("Hendra", 27, "Lombok", "Turu!"));