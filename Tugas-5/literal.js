//Soal No. 1 (Array to Object)
console.log("Array to Object")
function arrayToObject(arr) {
    for ( var i = 0; i < arr.length; i++) {
        var thisYear = (new Date().getFullYear()); 

        var personarr = arr[i];

        var personobj = {
            firstName : personarr[0],
            lastName : personarr[1],
            gender : personarr[2],
            age : personarr[3]
        }

        if (!personarr[3] || personarr[3] > thisYear) { //jika umur tidak sama (!) atau (||) lebih besar
            personobj.age = "Invalid Birth Yeear"
        } else {
            personobj.age = thisYear - personarr[3]
        }

        var fulName = personobj.firstName + " " + personobj.lastName;
        console.log(`${i + 1} . ${fulName} :` , personobj)

    }
}
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
];

var people2 = [ 
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023]
]
console.log("People:"), arrayToObject(people);
console.log()
console.log("People 2:"), arrayToObject(people2)
console.log()


//Soal No. 2 (Naik Angkot)
console.log("Naik Angkot")
function naikAngkot(arrPenumpang) {
    var rute = ["A", "B", "C", "D", "E", "F"]
    var output = []

    for (var p = 0; p < arrPenumpang.length; p++) {
        var penumpangS = arrPenumpang[p]
        var obj = {
            penumpang: penumpangS[0],
            naikDari: penumpangS[1],
            tujuan: penumpangS[2]
        }

        var bayar = (rute.indexOf(penumpangS[2]) - rute.indexOf(penumpangS[1])) * 2000
        obj.bayar = bayar
        output.push(obj)
    }
    return output
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]))

//Soal No. 3 Nilai Tertinggi

function nilaiTertinggi(siswa) {
    var output = {}
    
    for (var s = 0; s < siswa.length; s++) {
        var current = siswa[s]
        
       // if (current.score > output[current.class].score) {
          //xxxx
        //  }
    }
    return output       
}
  console.log(nilaiTertinggi([
  {
    name: 'Asep',
    score: 90,
    class: 'adonis'
  },
  {
    name: 'Ahmad',
    score: 85,
    class: 'vuejs'
  },
  {
    name: 'Regi',
    score: 74,
    class: 'adonis'
  },
  {
    name: 'Afrida',
    score: 78,
    class: 'reactjs'
  }
  ]));
  console.log("Maaf Min, sy bingung disana :-(, Tolong kasih jawaban untuk jadi pembelajaran. - Terima Kasih")