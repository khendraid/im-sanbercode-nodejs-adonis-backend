import { sapa, convert, checkScore, filterData } from "./libs/soal";
const myArgs = process.argv.slice(2);
const command = myArgs[0] 

switch (command) {
    case 'sapa':
        let nama = myArgs[1]
        console.log(sapa(nama));
        break;
    
    case 'convert':
        const parameter = myArgs.slice(1);
        let [Nama, Domisili, Umur] = parameter
        console.log(convert(Nama, Domisili, Umur));
        break;


    case 'filterData':
        let kelas = myArgs[1];
        console.log(filterData(kelas))
        break;

    case 'checkScore':
        const param = myArgs.slice(1)
        const output = String(param).split(/[,:]+/)
        let[name,kelass,score] = output
        console.log(checkScore(name,kelass,score))
        break;
    
    default:
    break;
}
