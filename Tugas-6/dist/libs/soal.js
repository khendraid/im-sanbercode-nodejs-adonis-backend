"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sapa = exports.filterData = exports.convert = exports.checkScore = void 0;

var sapa = function sapa(nama) {
  return "Halo Selamat Malam, ".concat(nama);
};

exports.sapa = sapa;

var convert = function convert(Nama, Domisili, Umur) {
  return {
    Nama: Nama,
    Domisili: Domisili,
    Umur: Umur
  };
};

exports.convert = convert;
var data = [{
  name: "Ahmad",
  kelas: "adonis"
}, {
  name: "Regi",
  kelas: "laravel"
}, {
  name: "Bondra",
  kelas: "adonis"
}, {
  name: "Iqbal",
  kelas: "vuejs"
}, {
  name: "Putri",
  kelas: "Laravel"
}];

var filterData = function filterData(kelas) {
  for (var i = 0; i < data.length; i++) {
    return data.filter(function (element) {
      return element['kelas'].toLowerCase().includes(kelas.toLowerCase());
    });
  }
};

exports.filterData = filterData;

var checkScore = function checkScore(name, kelass, score) {
  return {
    name: name,
    kelass: kelass,
    score: score
  };
};

exports.checkScore = checkScore;