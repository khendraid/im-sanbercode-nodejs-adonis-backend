// Soal No. 1 Membuat Kalimat
    var kata = "I";
    var kata2 = "am";
    var kata3 = "going";
    var kata4 = "to";
    var kata5 = "be";
    var kata6 = "React";
    var kata7 = "Native";
    var kata8 = "Developer"
        console.log("OutPut Membuat Kalimat");
        console.log(" " + kata + " " + kata2 + " " + kata3 + " " +kata4 + " " + kata5 + " " + kata6 + " " + kata7 + " " + kata8);
        console.log()
//Soal No. 2 Mengurai Kalimat (Akses karakter dalam string)
    var kalimat = "I am going to be React Native Developer";
    var word1 = kalimat[0];
    var word2 = kalimat[2] + kalimat[3];
    var word3 = kalimat[5] + kalimat[6] + kalimat[7] + kalimat[8] + kalimat[9];
    var word4 = kalimat[11] + kalimat [12];
    var word5 = kalimat[14] + kalimat[15];
    var word6 = kalimat[17] + kalimat [18] + kalimat[19] + kalimat[20] + kalimat[21] + kalimat[22];
    var word7 = kalimat[23] + kalimat [24] + kalimat[25] + kalimat[26] + kalimat[27] + kalimat[28];
    var word8 = kalimat[30] + kalimat[31] + kalimat[32] + kalimat[33] + kalimat[34] + kalimat[35] + kalimat[36] + kalimat[37] + kalimat[38];
        console.log("OutPut Mengurai Kalimat (String)");
        console.log(" " + "Kata Pertama:" + " " + word1);
        console.log(" " + "Kata Kedua:" + " " + word2);
        console.log(" " + "Kata Ketiga:" + " " + word3);
        console.log(" " + "Kata Keempat:" + " " + word4);
        console.log(" " + "Kata Kelima:" + " " + word5);
        console.log(" " + "Kata Keenam:" + " " + word6);
        console.log(" " + "Kata Ketujuh:" + " " + word7);
        console.log(" " + "Kata Kedelapan:" + " " + word8);
        console.log()

//Soal No. 3 Mengurai Kalimat (Substring)
    var kalimat2 = "wow JavaScript is so cool";
    var sword1 = kalimat2.substring(0,3);
    var sword2 = kalimat2.substring(4,14);
    var sword3 = kalimat2.substring(15,17);
    var sword4 = kalimat2.substring(18,20);
    var sword5 = kalimat2.substring(21,25);
        console.log("OutPut Mengurai Kalimat (Substring)");
        console.log(" " + "Kata Pertama:" + " " +  sword1);
        console.log(" " + "Kata Kedua:" + " " + sword2);
        console.log(" " + "Kata Ketiga:" + " " + sword3);
        console.log(" " + "Kata Keempat:" + " " + sword4);
        console.log(" " + "Kata Kelima:" + " " + sword5);
        console.log()

//Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
    var sword1Length = sword1.length;
    var sword2Length = sword2.length;
    var sword3Length = sword3.length;
    var sword4Length = sword4.length;
    var sword5Length = sword5.length;
        console.log("OutPut Mengurai Kalimat dan Menentukan Panjang String");
        console.log(" " + "Kata Pertama:" + " " + sword1 + ", With Length:" + " " + sword1Length);
        console.log(" " + "Kata Kedua:" + " " + sword2 + ", With Length:" + " " + sword2Length);
        console.log(" " + "Kata Ketiga:" + " " + sword3 + ", With Length:" + " " + sword3Length);
        console.log(" " + "Kata Keempat:" + " " + sword4 + ", With Length:" + " " + sword4Length);
        console.log(" " + "Kata Kelima:" + " " + sword5 + ", With Length:" + " " + sword5Length);
